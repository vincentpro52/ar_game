using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class DebugNavMesh : MonoBehaviour
{


   
    // Start is called before the first frame update
    void Start()
    {

    }


    public void debugNavMeshPlane(NavMeshData navMeshData)
    {

        Vector3 minVec = navMeshData.sourceBounds.min;
        Vector3 maxVec = navMeshData.sourceBounds.max;
        Vector3[] vertices = new Vector3[8]
            {
                minVec,
                new Vector3(maxVec.x, minVec.y, minVec.z),
                new Vector3(maxVec.x, maxVec.y, minVec.z),
                new Vector3(minVec.x, maxVec.y, minVec.z),
                new Vector3(minVec.x, maxVec.y, maxVec.z),
                maxVec,
                new Vector3(maxVec.x, minVec.y, maxVec.z),
                new Vector3(minVec.x, minVec.y, maxVec.z)
            };

        //Vector2[] m_UV;
        int[] triangles = {
            0, 2, 1, //face front
	        0, 3, 2,
            2, 3, 4, //face top
	        2, 4, 5,
            1, 2, 5, //face right
	        1, 5, 6,
            0, 7, 4, //face left
	        0, 4, 3,
            5, 4, 7, //face back
	        5, 7, 6,
            0, 6, 7, //face bottom
	        0, 1, 6
        };

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.Optimize();
        mesh.RecalculateNormals();
        //mesh.uv = newUV;
    }
}
