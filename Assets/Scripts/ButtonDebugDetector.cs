using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;

public class ButtonDebugDetector : MonoBehaviour
{

    private bool isButtonPressed = false;
    private bool switchBool = true;
    private GameObject gameObjectSessionOrigin;
    private ARPlaneManager m_ARPlaneManager;
    private PlayerScript playerScript;
    private GameObject gameObjectARSession;
    private Button button;
    private RectTransform rectTransform;

    private void Awake()
    {
        gameObjectARSession = GameObject.Find("AR Session");
        playerScript = gameObjectARSession.GetComponent<PlayerScript>();
    }

    private void OnEnable()
    {
        playerScript.OnStartTouch += IsTouchOnButton;
    }

    private void OnDisable()
    {
        playerScript.OnStartTouch -= IsTouchOnButton;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InitCouroutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IsTouchOnButton(Vector2 screenPosition, float time)
    {
        Vector2 positionButton = rectTransform.sizeDelta.normalized;
        if (screenPosition.normalized == positionButton )
        {
            Debug.Log("DEBUG: BUTTON DOWWWN IS WORKING");
            //Toggle bool back and forth
            switchBool = !switchBool;
            m_ARPlaneManager.planePrefab.SetActive(switchBool);
        }
    }

    //public void OnPointerDown(PointerEventData buttonEventData)
    //{
    //    Debug.Log("DEBUG: BUTTON DOWWWN IS WORKING");
    //    //Toggle bool back and forth
    //    switchBool = !switchBool;
    //    m_ARPlaneManager.planePrefab.SetActive(switchBool);
    //}

    //public void OnPointerUp(PointerEventData buttonEventData)
    //{
    //    Debug.Log("DEBUG: BUTTON UUUUP IS WORKING");
    //}


    /* Coroutine */
    IEnumerator InitCouroutine()
    {
        yield return new WaitForEndOfFrame();
        gameObjectSessionOrigin = GameObject.Find("AR Session Origin");
        m_ARPlaneManager = gameObjectSessionOrigin.GetComponent<ARPlaneManager>();
        button = GetComponent<Button>();
        rectTransform = button.GetComponent<RectTransform>();
    }

}
