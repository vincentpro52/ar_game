using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


public class PlayerScript : MonoBehaviour
{
    /* Event that other script can subscribe to */
    public delegate void StartTouchEvent(Vector2 position, float time);
    public event StartTouchEvent OnStartTouch;
    public delegate void EndTouchEvent(Vector2 position, float time);
    public event EndTouchEvent OnEndTouch;

    private ARChess inputARChess;
    private GameObject gameObjectSession;
    private ARPlaneManager m_ARPlaneManager;
    private ARRaycastManager m_ARRaycastManager;
    private AddObject addObject;
    private ARSessionOrigin m_ARSessionOrigin;

    private void Awake()
    {
        inputARChess = new ARChess();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(initCouroutine());
        inputARChess.Player.TouchPress.started += ctx => StartTouch(ctx);
        inputARChess.Player.TouchPress.canceled += ctx => EndTouch(ctx);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnEnable()
    {
        inputARChess.Enable();
    }

    private void OnDisable()
    {
        inputARChess.Disable();
    }


    private void StartTouch(InputAction.CallbackContext context)
    {
        Debug.Log("<color=green><b>TOUCH STARTED: </b></color>" + $"<color=yellow><b>{inputARChess.Player.TouchPosition.ReadValue<Vector2>()}</b></color>");

        if (!Application.isEditor)
        {
            List<ARRaycastHit> hitObject = new List<ARRaycastHit>();
            if (m_ARRaycastManager.Raycast(inputARChess.Player.TouchPosition.ReadValue<Vector2>(), hitObject, TrackableType.PlaneWithinPolygon))
            {
                Debug.Log("<color=green><b>HIT</b></color>");
                hitObject.ForEach(HandleRaycast);
                foreach (var plane in m_ARPlaneManager.trackables)
                {
                    plane.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            debugLogic();
        }

        
        if (OnStartTouch != null)
        {
            OnStartTouch(inputARChess.Player.TouchPosition.ReadValue<Vector2>(), (float)context.startTime);
        }
    }

    private void EndTouch(InputAction.CallbackContext context)
    {
        Debug.Log("<color=green><b>TOUCH ENDED</b></color>");
        if (OnEndTouch != null)
        {
            OnEndTouch(inputARChess.Player.TouchPosition.ReadValue<Vector2>(), (float)context.time);
        }
    }

    private void HandleRaycast(ARRaycastHit hit)
    {
        if (hit.trackable is ARPlane plane)
        {
            Debug.Log($"Hit a plane with alignment {plane.alignment}");
            var instObject = addObject.AddPrefab(hit.pose.position, hit.pose.rotation);
            //m_ARSessionOrigin.MakeContentAppearAt(instObject.transform, instObject.transform.position, instObject.transform.rotation);
        }
    }
    

    /* Coroutine */
    IEnumerator initCouroutine()
    {
        yield return new WaitForEndOfFrame();
        gameObjectSession = GameObject.Find("AR Session Origin");
        m_ARSessionOrigin = gameObjectSession.GetComponent<ARSessionOrigin>();
        m_ARPlaneManager = gameObjectSession.GetComponent<ARPlaneManager>();
        m_ARRaycastManager = gameObjectSession.GetComponent<ARRaycastManager>();
        addObject = GameObject.Find("VirtualObject").GetComponent<AddObject>();
    }



    /* Debug */
    private void debugLogic()
    {
        Camera camera = gameObjectSession.transform.GetChild(0).GetComponent<Camera>();
        RaycastHit hit;
        Vector3 clickPosition = inputARChess.Player.TouchPosition.ReadValue<Vector2>();
        Ray ray = camera.ScreenPointToRay(clickPosition);
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("<color=green><b>HIT</b></color>");
            var instObject = addObject.AddPrefab(hit.transform.position, hit.transform.rotation);
        }
        else
        {
            Debug.Log("<color=green><b>NO HIT</b></color>");
        }
    }


}
