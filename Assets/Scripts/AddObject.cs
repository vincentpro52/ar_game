using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.AI;
public class AddObject : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabInst;
    [SerializeField]
    private GameObject damierInst;
    [SerializeField]
    private List<GameObject> whitePieces;
    [SerializeField]
    private GameObject submarine;

    /* **************** */
    private bool isBoardInst = false;
    private GameObject boardIsland;
    private GameObject damierBoard;
    private GameObject subInst;
    private NavMeshSurface navMeshBoard;
    private List<GameObject> instwhitePieces;


    private void Awake()
    {
        navMeshBoard = GetComponent<NavMeshSurface>();
        instwhitePieces = new List<GameObject>();
    }

    /* Start is called before the first frame update */
    void Start()
    {
        if (Application.isEditor)
        {
            Debug.Log("<color=green><b>Application Running in Editor</b></color>");
        }
    }

    /* Update is called once per frame */
    void Update()
    {
        
    }

    public GameObject AddPrefab(Vector3 position, Quaternion rotation) 
    {
        if (!isBoardInst)
        {
            isBoardInst = true;
            boardIsland = Instantiate(prefabInst, position, rotation);
            boardIsland.transform.Rotate(0.0f, 90.0f, 0.0f, Space.Self);
            //damierBoard = Instantiate(damierInst, position, rotation);
            if (boardIsland.GetComponent<ARAnchor>() == null)
            {
                boardIsland.AddComponent<ARAnchor>();
                Debug.Log("<color=green><b>ANCHOR ADDED BOARDISLAND</b></color>");
            }
            //if (damierBoard.GetComponent<ARAnchor>() == null)
            //{
            //    damierBoard.AddComponent<ARAnchor>();
            //    Debug.Log("<color=green><b>ANCHOR ADDED DAMIER</b></color>");
            //}
            if (boardIsland.GetComponent<NavMeshSurface>() == null)
            {
                NavMeshSurface navMeshSurfBoard = boardIsland.AddComponent<NavMeshSurface>();
                navMeshSurfBoard.agentTypeID = 0;
                navMeshSurfBoard.collectObjects = CollectObjects.All;     
                navMeshSurfBoard.useGeometry = NavMeshCollectGeometry.PhysicsColliders;
                navMeshSurfBoard.defaultArea = 0;        // Walkable = 0
                navMeshSurfBoard.ignoreNavMeshAgent = true;
                navMeshSurfBoard.overrideVoxelSize = true;
                navMeshSurfBoard.voxelSize = 0.2f;
                navMeshSurfBoard.overrideTileSize = true;
                navMeshSurfBoard.tileSize = 128;
                navMeshSurfBoard.center = boardIsland.transform.InverseTransformPoint(boardIsland.GetComponent<MeshRenderer>().bounds.center);

                Vector3 volumeNav = boardIsland.GetComponent<MeshRenderer>().bounds.size * 1.1f;
                navMeshSurfBoard.size = new Vector3(volumeNav.x, 1, volumeNav.z);
                navMeshSurfBoard.BuildNavMesh();

                //boardIsland.gameObject.SetActive(false);


                /* DEBUG */
                Debug.Log("navMeshSurfaceBoard.CENTER: " + $"{navMeshSurfBoard.center.ToString("F4")}");
                Debug.Log("volumeNav SIZE: " + $"{volumeNav.ToString("F4")}");
                Debug.Log("navMeshSurfaceBoard.SIZE: " + $"{navMeshSurfBoard.size.ToString("F4")}");
                Debug.Log("NAV MESH SIZE: " + $"{navMeshSurfBoard.navMeshData.sourceBounds.size.ToString("F4")}");
                //GameObject debugNavMesh = GameObject.Find("debugNavMesh");
                //GameObject debugInst = Instantiate(debugNavMesh, navMeshSurfBoard.navMeshData.position, navMeshSurfBoard.navMeshData.rotation); 
                //DebugNavMesh debugNavMeshPlane = debugInst.AddComponent<DebugNavMesh>();
                //debugNavMeshPlane.debugNavMeshPlane(navMeshSurfBoard.navMeshData);
                Debug.Log("<color=green><b>NavMesh ADDED</b></color>");
                /* END DEBUG */
            }
            //AddPieces();
            AddSubmarine();
            StartCoroutine("MoveSubmarine");
            
            return boardIsland;
        }
        else
        {
            return boardIsland;
        }
    }

    private void AddPieces()
    {
        Transform kingPos = boardIsland.transform.GetChild(0);
        foreach (GameObject oneWhitePiece in whitePieces)
        {
            if (oneWhitePiece.name == "King" || oneWhitePiece.name == "TyphoonPref")
            {
                instwhitePieces.Add(Instantiate(oneWhitePiece, kingPos.position, kingPos.rotation));
                foreach (GameObject oneInstWhiteP in instwhitePieces)
                {
                    if (oneInstWhiteP.GetComponent<ARAnchor>() == null)
                    {
                        oneInstWhiteP.AddComponent<ARAnchor>();
                        Debug.Log("<color=green><b>ANCHOR KING ADDED:</b></color>");
                    }
                    NavMeshAgent navMeshAgent = oneInstWhiteP.GetComponent<NavMeshAgent>();
                    navMeshAgent.baseOffset = -0.963f;
                }
            }
        }
    }

    private void AddSubmarine()
    {
        Transform submarinePosCenter = boardIsland.transform.GetChild(2);
        if (submarinePosCenter.name == "SubmarinePosCenter")
        {
            subInst = Instantiate(submarine, submarinePosCenter.position, submarinePosCenter.rotation);
            if (subInst.GetComponent<ARAnchor>() == null)
            {
                subInst.AddComponent<ARAnchor>();
                Debug.Log("<color=green><b>Submarine Anchor ADDED:</b></color>");
            }
        }
    }

    private IEnumerator MoveSubmarine()
    {
        for (int i = 0; i < 1000; i++)
        {
            subInst.transform.Translate(subInst.transform.forward * Time.deltaTime/10);
            yield return new WaitForSeconds(0f);
        }
        // Wrong axis of translation due to rotation 90 degree from boardIsland
        // Check submarine transform in local space, maybe rotate prefab
    }

}
